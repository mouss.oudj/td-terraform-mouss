# terraform_td

First, replace the **<YOUR_BUCKET_NAME>** in the files/policy.json and the variables.tf file by a unique bucket name.

Then run:

```bash
terraform init
```

after your terraform has been initialized properly you can run :

```bash
terraform apply
```


don't forget to destroy your resources afterword with :

```bash
terraform destroy
```
