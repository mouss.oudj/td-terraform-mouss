resource "aws_s3_bucket" "bucket_web_hosting" {
  bucket = var.bucket_name
  acl    = "public-read"
  policy = file("files/policy.json")
  force_destroy = true
  website {
    index_document = "index.html"

  }
}

resource "aws_s3_bucket_object" "index_html" {
  bucket = aws_s3_bucket.bucket_web_hosting.bucket
  key    = "index.html"
  source = "files/index.html"

  content_type = "text/html"
}